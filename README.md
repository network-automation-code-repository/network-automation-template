# **Project**: "name of the project"
### Team X: "first name last name, etc."

## **Description of the project**

Describe your project (max 250 words): goal, development, tools, etc.

## **Description of the repository**


- **_main.py_**: 
This file contains ...


- **_lib/_**: This folder contains ...

    - **_lib/utils.py_**: ...
    - **_lib/program.py_**: ...


## **How to run the project**

Describe how to clone the project and run it. Specify commands, etc.

## **Results**

Describe the main results of your project (max 250 words)

